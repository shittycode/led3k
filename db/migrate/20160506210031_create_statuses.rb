class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.string :thread

      t.timestamps null: false
    end
  end
end
