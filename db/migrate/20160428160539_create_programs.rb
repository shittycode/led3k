class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.text :title
      t.text :code
      t.text :failed

      t.timestamps null: false
    end
  end
end
