json.array!(@programs) do |program|
  json.extract! program, :id, :title, :code, :failed
  json.url program_url(program, format: :json)
end
