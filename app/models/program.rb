class Program < ActiveRecord::Base
  validates :code,:title, presence: true
  validates :title, uniqueness: true
end
