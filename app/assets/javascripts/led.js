$(document).on('change', '.color-range', function (e) {
   showVal(this);
   $('.control-form').submit();
    lastval = false;
});

tolerance = 20;
lastval = false;
ready = true;
$(document).on('input', '.color-range', function (e) {
  showVal(this);
  if(!lastval){
  lastval = this.value;
  }else{
	  if((this.value > lastval+tolerance || this.value < lastval -tolerance) && ready){
	  lastval = false;
	  	 ready = false;
	  	$('.control-form').submit();
	  	 
 	 }
  }
  
});


function showVal(target){
	$(target).next('.range-value').html(target.value);
}