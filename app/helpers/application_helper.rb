module ApplicationHelper
  require 'yaml'
  
  def self.clamp min, max,val
    [[val, max].min, min].max
  end
  
  def self.color(r,g,b)
    arr = [r,g,b]
    arr[0]= clamp(0,255,arr[0])
    arr[1]= clamp(0,255,arr[1])
    arr[2]= clamp(0,255,arr[2])
    File.open("color",'w'){|file| file.puts(YAML.dump(arr))}
  end
  
  def self.colorarray(a)
    a[0]= clamp(0,255,a[0])
    a[1]= clamp(0,255,a[1])
    a[2]= clamp(0,255,a[2])
     File.open("color",'w'){|file| file.puts(YAML.dump(a))}
  end
  
  #this is a clusterfuck, do not attemt to understand this
  def self.execute(program)
    # byebug
    program.failed = nil

    ActiveRecord::Base.connection.close
    Status.new().save if Status.first.nil? 
    
    Thread.list.each do |thread|
      
        thread.exit if thread.to_s == Status.first.thread
        
    end
    
   thread = Thread.new do
      begin
        eval(program.code)
      rescue Exception => e
      program.failed = e.to_s
      end
    end
    Status.first.destroy
    status = Status.new
    status.thread  = thread.to_s
     status.save

    sleep(0.1)
    program.save
  end
end
