class ProgramsController < ApplicationController
  before_action :set_program, only: [:update, :destroy]
  def execute

    ApplicationHelper.execute(Program.find(params[:program_id]))

    @programs = Program.all
    @active = params[:program_id]
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end
  
  def index
    
    @programs = Program.all
    @program = Program.new
  end

  # POST /programs
  # POST /programs.json
  def create
    Program.all.each do |p|
      if p.title == program_params[:title]
        @program = p
      end
    end
 if @program.nil?
   
        @program = Program.new(program_params)
        if @program.save
          ApplicationHelper.execute(@program)
        end
      else
        if @program.update(program_params)
          ApplicationHelper.execute(@program)
        end
      end
    @active = @program.id

    respond_to do |format|
     
      @programs = Program.all
      format.js
    end
  end

  def edit
    @program = Program.find(params[:id])
    puts @program.code
     @active = @program.id
    respond_to do |format|
      format.js
    end
  end

  # PATCH/PUT /programs/1
  # PATCH/PUT /programs/1.json
  def update
    respond_to do |format|
      if @program.update(program_params)
      # format.html { redirect_to :back}

      else
      #  format.html { redirect_to :back}
      end
      execute
      @programs = Program.all
      format.js
    end
  end

  # DELETE /programs/1
  # DELETE /programs/1.json
  def destroy
    @program.destroy
    @programs = Program.all
    respond_to do |format|
      format.html { redirect_to :back }
      format.js
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_program
    @program = Program.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def program_params
    params.require(:program).permit(:title, :code, :failed)
  end
end
